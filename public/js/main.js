document.getElementById('burger-toggle').onclick = function() {
    toggleBurgerMenu();
};

document.getElementById('content-overlay').onclick = function() {
    toggleBurgerMenu();
};

function toggleBurgerMenu() {
    this.__toggle = !this.__toggle;
    var target = document.getElementById('sidebar');
    if( this.__toggle) {
        target.classList.add("sidebar-open");
        document.getElementById('content-overlay').classList.add("content-overlay-show");
    }
    else {
        target.classList.remove("sidebar-open");
        document.getElementById('content-overlay').classList.remove("content-overlay-show");
    }
}