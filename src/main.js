import Vue from 'vue'
import App from './App.vue'
import VModal from 'vue-js-modal'
import {store} from './store';
import * as axios from 'axios'
import router from './router';
import env from "./configs/env";

import VueCarousel from 'vue-carousel';

Vue.use(VModal);

Vue.use(VueCarousel);

Vue.config.productionTip = false;
Vue.prototype.$axios = axios;
const token = localStorage.getItem('token')
if (token) {
    axios.defaults.headers.common['Authorization'] = token
}
axios.defaults.baseURL = env.backendUrl;
new Vue({
    render: h => h(App),
    router,
    store,
}).$mount('#app');