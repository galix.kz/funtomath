import Vue from 'vue'
import Router from 'vue-router'
import App from '../App'
import News from "../components/pages/News";
import Main from "../components/pages/Main";
import Faq from "../components/pages/Faq";
import Favorite from "../components/pages/Favorite";
import NewsList from "../components/pages/NewsList";
import Settings from "../components/pages/Settings";
import Sections from "../components/pages/Sections";
import Task from "../components/pages/Task";
import Categories from "../components/pages/Categories";
import Books from "../components/pages/Books";
import TestSubCategory from "../components/pages/TestSubCategory";
import TestStart from "../components/pages/TestStart";
import Testing from "../components/pages/Testing";
import TestFinish from "../components/pages/TestFinish";

Vue.use(Router)

let router = new Router({
    mode: 'hash',
    routes: [
        {
            path: '/',
            name: 'Index',
            component: Main
        },
        {
            path: '/news',
            name: 'NewsList',
            component: NewsList,
            meta: {
                guest: true
            }
        },
        {
            path: '/news/:id',
            name: 'NewsDetailed',
            component: News,
            props: true,
            meta: {
                guest: true
            }
        },
        {
            path: '/faq',
            name: 'FAQ',
            component: Faq,
            meta: {
                guest: true
            }
        },
        {
            path: '/favorite',
            name: 'Favorites',
            component: Favorite,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/settings',
            name: 'Settings',
            component: Settings,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/task/:id',
            name: 'Task',
            component: Task,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/category/:id',
            name: 'Category',
            component: Categories,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/books/:id',
            name: 'Books',
            component: Books,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/section/:id',
            name: 'Sections',
            component: Sections,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/test-category/:id',
            name: 'TestSubCategory',
            component: TestSubCategory,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/test/:id',
            name: 'Test',
            component: Testing,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/test-results/',
            name: 'TestResults',
            component: TestFinish,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
    ]
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('token') == null) {
            next({
                path: '/',
                params: { nextUrl: to.fullPath }
            })
        }
        else{
            next(window.scrollTo(0,0))
        }
        // else {
        //     let user = JSON.parse(localStorage.getItem('user'))
        //     if(to.matched.some(record => record.meta.is_admin)) {
        //         if(user.is_admin == 1){
        //             next()
        //         }
        //         else{
        //             next({ name: 'userboard'})
        //         }
        //     }else {
        //         next()
        //     }
        // }
    }
    else {
        next()
    }
})

export default router
