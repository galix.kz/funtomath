import axios from "axios";
import env from "../configs/env";

export default  {
    state: {
        testSections: '',
        subCategoryTests: '',
        testQuestions: '',
        listOfAnswers: '',
        results: '',
        estimatedTime:0,
    },
    mutations: {
        set_testSections(state, subject){
            state.testSections = subject
        },
        set_subCategoryTests(state, subject){
            state.subCategoryTests = subject
        },
        set_testQuestions(state, subject){
            state.testQuestions = subject
            let arr = []
            subject.answers.forEach(()=>{
                arr.push([])
            })
            state.listOfAnswers = arr
        },
        set_results(state, subject){
            console.log(subject[0])
            state.results = subject[0]
            state.listOfAnswers = subject[1];
            state.estimatedTime = subject[2]
        },
    },
    getters:{
        testSections: state => state.testSections,
        subCategoryTests: state => state.subCategoryTests,
        testQuestions: state => state.testQuestions,
        listOfAnswers: state => state.listOfAnswers,
        results: state => state.results,
        estimatedTime: state => state.estimatedTime,
    },
    actions: {
        loadTaskSections({commit}, subjectId){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.get( env.backendUrl+'/challenge/sections/'+subjectId)
                    .then(resp => {
                        commit('set_testSections', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
        loadSubCategoryTests({commit}, subCategoryId){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.get( env.backendUrl+'/challenge/tests/'+subCategoryId)
                    .then(resp => {
                        console.log(resp)
                        commit('set_subCategoryTests', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
        loadTestQuestions({commit}, testId){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.get( env.backendUrl+'/challenge/test/'+testId)
                    .then(resp => {
                        console.log(resp)
                        let testQuestions = {};
                        testQuestions['answers'] = resp.data.data;
                        testQuestions['extra'] = resp.data.extra
                        commit('set_testQuestions', testQuestions)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
        finishTest({commit}, resultsArray){

            return new Promise((resolve, reject) => {

                commit('auth_request')
                axios.post( env.backendUrl+'/challenge/test/result/', {data: resultsArray.answers})
                    .then(resp => {
                        commit('set_results', [resp.data.data, resultsArray.answers, resultsArray.estimatedTime])
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
    }
}
