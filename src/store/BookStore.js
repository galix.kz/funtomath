import axios from "axios";
import env from "../configs/env";

export default  {
    state: {
        subjects: '',
        books: '',
        sections: '',
        tasks: '',
    },
    mutations: {
        set_subjects(state, subject){
            state.subjects = subject
        },
        set_books(state, books){
            state.books = books
        },
        set_sections(state, sections){
            state.sections = sections
        },
        set_tasks(state, tasks){
            state.tasks = tasks
        }
    },
    getters:{
      subjects: state => state.subjects,
        books: state => state.books,
        sections: state => state.sections,
        tasks: state => state.tasks
    },
    actions: {
        loadSubjects({commit}, subjectId){
            return new Promise((resolve, reject) => {
                commit('set_subjects','')
                axios.get( env.backendUrl+'/subjects/'+subjectId)
                    .then(resp => {
                        commit('set_subjects', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
        loadBooks({commit}, subjectId){
            return new Promise((resolve, reject) => {
                commit('set_books','')
                axios.get( env.backendUrl+'/subject/'+subjectId+'/books')
                    .then(resp => {
                        console.log(resp)
                        commit('set_books', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
        loadSections({commit}, sectionsId){
            return new Promise((resolve, reject) => {
                commit('set_sections', '')
                axios.get( env.backendUrl+'/book/'+sectionsId)
                    .then(resp => {
                        console.log('sec', resp)
                        let data =resp.data.data;
                        data['extra'] = resp.data.extra
                        commit('set_sections', data)
                        resolve(resp)
                    })
                    .catch(error => {
                        commit('auth_error')
                        if (error.response) {
                            // Request made and server responded
                            console.log(error.response.data)
                            this.error = error.response.data
                            console.log(error.response.status)
                            console.log(error.response.headers)
                        } else if (error.request) {
                            // The request was made but no response was received
                            console.log(error.request)
                        } else {
                            // Something happened in setting up the request that triggered an Error
                            console.log('Error', error.message)
                        }
                        // reject(err)
                    })
            })
        },
        loadTask({commit}, taskId){
            return new Promise((resolve, reject) => {
                commit('set_tasks', '')
                axios.get( env.backendUrl+'/task/'+taskId)
                    .then(resp => {
                        let data =resp.data.data;
                        console.log('asdasd', )
                        commit('set_tasks', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        }
    }
}
