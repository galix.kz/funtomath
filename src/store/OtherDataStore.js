import axios from "axios";
import env from "../configs/env";

export default  {
    state: {
        favorites: '',
        news: '',
    },
    mutations: {
        set_favorites(state, subject){
            state.favorites = subject
        },
        remove_from_favorite(state, id){
            let favorites = state.favorites;
            let newarr = favorites.filter((value, index, favorites)=>{
                console.log(value.id !== id)
                return value.id !== id;
            })
            console.log(newarr)
            state.favorites = newarr;
        },
        set_news(state, news){
            state.news = news
        }
    },
    getters:{
        favorites: state => state.favorites,
        news: state=> state.news,
    },
    actions: {
        loadFavorites({commit}) {
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.get(env.backendUrl + '/profile/favorites')
                    .then(resp => {
                        commit('set_favorites', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
        removeFromFavorite({commit}, id) {
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.get(env.backendUrl + '/profile/remove-from-favorites/' + id)
                    .then(resp => {
                        commit('remove_from_favorite', id)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
        loadNews({commit}, id) {
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.get(env.backendUrl + '/news')
                    .then(resp => {
                        commit('set_news', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
        loadNewsDetail({commit}, id) {
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.get(env.backendUrl + '/news/'+id)
                    .then(resp => {
                        commit('set_news', resp.data.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
    }
}
