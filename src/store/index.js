import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import env from '../configs/env';
import bookStore from './BookStore';
import testStore from './TestStore';
import otherStore from './OtherDataStore'
Vue.use(Vuex);
axios.defaults.headers.common['Authorization'] = localStorage.getItem('token') || '';
const indexStore = {
    state: {
        token: localStorage.getItem('token') || '',
        user: '',
        phone: '',
        status: '',
        error: '',
        deviceId: localStorage.getItem('deviceId') || '',
        regions: '',
        cities: '',
        schools: '',
    },
    getters: {
        isAuthenticated: state => !!state.token,
        user: state => state.user,
        authStatus: state => state.status,
        getError: state => state.error,
        getPhone: state => state.phone,
        regions: state => state.regions,
        cities: state => state.cities,
        schools: state => state.schools,
    },
    mutations: {
        auth_request(state){
            state.status = 'loading'
        },
        auth_success(state, token, user){
            state.status = 'success'
            state.token = token
            state.user = user
        },
        validate_success(state, phone){
          state.phone = phone
        },
        set_data(state, dataName, data){
          state[dataName] = data;
        },
        set_region(state, data){
            state.regions = data;
        },
        set_cities(state, data){
            state.cities = data;
        },
        set_schools(state, data){
            state.schools = data;
        },
        set_location_info_to_none(state, location){
            if(location==="region"){
                state.regions = '';
                state.cities = "";
            }
            else if(location==="city"){
                state.cities = "";
            }
            state.schools = '';
        },
        set_profile(state, data){
            state.user = data;
        },
        auth_error(state, error){
            state.status = 'error'
            state.error = error
        },
        logout(state){
            state.status = ''
            state.token = ''
        },
    },
    actions: {
        login({commit}, data){
            return new Promise((resolve, reject) => {
                commit('auth_request')

                let deviceId = localStorage.getItem('deviceId')
                axios.post(env.backendUrl+'/auth/login', {
                    phone_number: data.phone,
                    password: data.password,
                    device_id: deviceId,
                    language: 'rus'
                })
                    .then(resp => {
                        if (resp.data.status) {
                            const token = 'Token '+resp.data.data.token
                            const user = resp.data.data.user.profile
                            console.log(resp)
                            localStorage.setItem('token', token)
                            axios.defaults.headers.common['Authorization'] = token
                            commit('auth_success', token, user)
                        } else {
                            commit('auth_error', resp.data.message)
                            reject(resp.data.message)
                        }

                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
        validate_phone({commit}, phone){
            return new Promise((resolve, reject) => {
                console.log(phone)
                commit('auth_request')
                axios.post(env.backendUrl+'/auth/validate-number', {
                        phone_number: phone,
                        language: 'rus'
                    })
                    .then(resp => {
                        console.log(resp.data)
                        if (resp.data.status) {
                            console.log(phone)
                            commit('validate_success', phone)
                        } else {
                            commit('auth_error', resp.data.message)
                            reject(resp.data.message)
                        }
                        resolve(resp)
                    })
                    .catch(error => {
                        commit('auth_error', "Просим извинения ошибка на сервере")
                        // console.log('error', err )
                        if (error.response) {
                            // Request made and server responded
                            console.log(error.response.data)
                            this.error = error.response.data
                            console.log(error.response.status)
                            console.log(error.response.headers)
                        } else if (error.request) {
                            // The request was made but no response was received
                            console.log(error.request)
                        } else {
                            // Something happened in setting up the request that triggered an Error
                            console.log('Error', error.message)
                        }

                        reject(error)
                    })
            })
        },
        validate_phone_reset_password({commit}, phone){
            return new Promise((resolve, reject) => {
                console.log(phone)
                commit('auth_request')
                axios.post(env.backendUrl+'/auth/restore/validate-number', {
                    phone_number: phone,
                    language: 'rus'
                })
                    .then(resp => {
                        console.log(resp.data)
                        if (resp.data.status) {
                            console.log(phone)
                            commit('validate_success', phone)
                        } else {
                            commit('auth_error', resp.data.message)
                            reject(resp.data.message)
                        }
                        resolve(resp)
                    })
                    .catch(error => {
                        commit('auth_error', "Просим извинения ошибка на сервере")
                        // console.log('error', err )
                        if (error.response) {
                            // Request made and server responded
                            console.log(error.response.data)
                            this.error = error.response.data
                            console.log(error.response.status)
                            console.log(error.response.headers)
                        } else if (error.request) {
                            // The request was made but no response was received
                            console.log(error.request)
                        } else {
                            // Something happened in setting up the request that triggered an Error
                            console.log('Error', error.message)
                        }
                        // localStorage.removeItem('token')
                        reject(error)
                    })
            })
        },
        confirm_phone({commit, state}, code){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                console.log(state.phone)
                axios.post(env.backendUrl+'/auth/validate-code', {
                    phone_number: state.phone,
                    validate_code: code,
                    language: 'rus'
                })
                    .then(resp => {
                        // const token = resp.data.token
                        // const user = resp.data.user
                        // commit('validate_success', token, user)
                        console.log(resp.data)
                        if (resp.data.status) {

                        } else {
                            commit('auth_error', resp.data.message)
                            reject(resp.data.message)
                        }
                        resolve(resp)
                    })
                    .catch(error => {
                        commit('auth_error', "Просим извинения ошибка на сервере")
                        // console.log('error', err )
                        if (error.response) {
                            // Request made and server responded
                            console.log(error.response.data)
                            this.error = error.response.data
                            console.log(error.response.status)
                            console.log(error.response.headers)
                        } else if (error.request) {
                            // The request was made but no response was received
                            console.log(error.request)
                        } else {
                            // Something happened in setting up the request that triggered an Error
                            console.log('Error', error.message)
                        }
                        // localStorage.removeItem('token')
                        reject(error)
                    })
            })
        },
        register({commit, state}, password){
            console.log(password)
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.post(env.backendUrl+'/auth/register', {
                    phone_number: state.phone,
                    password: password,
                    language: 'rus'
                })
                    .then(resp => {
                        console.log(resp.data)
                        if (resp.data.status) {
                            axios.post(env.backendUrl+'/auth/login', {
                                phone_number: state.phone,
                                password: password,
                                device_id: state.deviceId,
                                language: 'rus'
                            })
                                .then(resp => {
                                    console.log(resp.data)
                                    if (resp.data.status) {
                                        const token = 'Token '+resp.data.data.token
                                        localStorage.setItem('token', token)
                                        axios.defaults.headers.common['Authorization'] = token
                                        commit('auth_success', token)
                                        resolve(resp)
                                    }
                                     else {
                                        commit('auth_error', resp.data.message)
                                        reject(resp.data.message)
                                    }})
                                .catch(err => {
                                        commit('auth_error', err)
                                        // localStorage.removeItem('token')
                                        reject(err)
                                    })
                        } else {
                            commit('auth_error', resp.data.message)
                            reject(resp.data.message)
                        }
                    })
                    .catch(err => {
                        commit('auth_error', err)
                        // localStorage.removeItem('token')
                        reject(err)
                    })
            })
        },

        reset_password({commit, state}, password){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.post(env.backendUrl+'/auth/restore/password-reset', {
                    phone_number: state.phone,
                    password: password,
                    language: 'rus'
                })
                    .then(resp => {
                        console.log(resp.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error', err)
                        // localStorage.removeItem('token')
                        reject(err)
                    })
            })
        },
        logout({commit}){
            return new Promise((resolve, reject) => {
                commit('logout')
                localStorage.removeItem('token')
                delete axios.defaults.headers.common['Authorization']
                resolve()
            })
        },
        loadRegions({commit}){
            return new Promise((resolve, reject) => {
                commit('set_location_info_to_none', "region")
                axios.get( env.backendUrl+'/region')
                    .then(resp => {
                        console.log(resp.data)
                        commit('set_region',  resp.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },
        loadCities({commit}, regionId){
            return new Promise((resolve, reject) => {
                commit('set_location_info_to_none', "city")
                axios.get( env.backendUrl+'/region/'+regionId+'/cities')
                    .then(resp => {
                        console.log(resp)
                        commit('set_cities',resp.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')

                        reject(err)
                    })
            })
        },
        loadSchools({commit}, cityId){
            return new Promise((resolve, reject) => {
                commit('set_location_info_to_none')
                axios.get( env.backendUrl+'/city/'+cityId+'/schools')
                    .then(resp => {
                        commit('set_schools', resp.data)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        reject(err)
                    })
            })
        },
        loadProfile({commit}){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.get( env.backendUrl+'/profile')
                    .then(resp => {
                        commit('set_profile', resp.data.data)
                        resolve(resp)
                    })
                    .catch(error => {
                        commit('auth_error')
                        if (error.response) {
                            // Request made and server responded
                            console.log(error.response.data)
                            this.error = error.response.data
                            if(error.response.status===401){
                                localStorage.removeItem('token')
                            }

                        } else if (error.request) {
                            // The request was made but no response was received
                            console.log(error.request)
                        } else {
                            // Something happened in setting up the request that triggered an Error
                            console.log('Error', error.message)
                        }

                        reject(err)
                    })
            })
        },
        updateProfile({commit},data){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.put( env.backendUrl+'/profile/update', data,
                    {headers : {
                        'Content-Type': 'multipart/form-data'
                    }})
                    .then(resp => {
                        console.log(resp)
                        if (resp.data.status) {
                            commit('set_profile', resp.data.data)
                            resolve(resp)
                        }
                        else {
                            commit('auth_error', resp.data.message)
                            reject(resp.data.message)
                        }})
                    .catch(error => {
                        commit('auth_error')
                        if (error.response) {
                            // Request made and server responded
                            console.log(error.response.data)
                            this.error = error.response.data
                            if(error.response.status===401){
                                localStorage.removeItem('token')
                            }

                        } else if (error.request) {
                            // The request was made but no response was received
                            console.log(error.request)
                        } else {
                            // Something happened in setting up the request that triggered an Error
                            console.log('Error', error.message)
                        }

                        reject(error)
                    })
            })
        },
        resetSettingsPassword({commit},password){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios.put( env.backendUrl+'/profile/password-reset', {
                    password: password
                    })
                    .then(resp => {
                        console.log(resp)
                        if (resp.data.status) {
                            commit('set_profile', resp.data.data)
                            resolve(resp)
                        }
                        else {
                            commit('auth_error', resp.data.message)
                            reject(resp.data.message)
                        }})
                    .catch(error => {
                        commit('auth_error')
                        if (error.response) {
                            // Request made and server responded
                            console.log(error.response.data)
                            this.error = error.response.data
                            if(error.response.status===401){
                                localStorage.removeItem('token')
                            }

                        } else if (error.request) {
                            // The request was made but no response was received
                            console.log(error.request)
                        } else {
                            // Something happened in setting up the request that triggered an Error
                            console.log('Error', error.message)
                        }

                        reject(error)
                    })
            })
        }
    }
}
export const store = new Vuex.Store({
    modules:{
    index: indexStore,
    bookStore: bookStore,
        testStore: testStore,
        otherStore: otherStore,
    }
})
